#!/usr/bin/perl -w

use v5.28;
use utf8;
use strict;
use warnings;

use Test::More;
use Mojo::URL;
use Mojo::DOM;

use_ok('App::PtLink::Input');

my $url = Mojo::URL->new('file://./t/test-files/valhalla.xml');

my $input = App::PtLink::Input->create("XmlFeed", url => $url);

isa_ok($input, "App::PtLink::Input");

my $messages = $input->messages;

my $dom = Mojo::DOM->new($messages->[0]->body->text);

ok($dom->find('img[src]')->first->attr('src') eq 'https://blog.trueelena.org/blog/2023/05/26-late_victorian_combinations/combinations_front.jpg', "The image URL is normalized correctly");

$url = Mojo::URL->new('file://./t/test-files/ikiwiki-grep.atom');
$input = App::PtLink::Input->create("XmlFeed", url => $url);
$messages = $input->messages;
$dom = Mojo::DOM->new($messages->[0]->body->text);
ok($dom->find('a[href]')->first->attr('href') eq 'https://grep.be/blog/en/computer/Backing_up_my_home_server_with_Bacula_and_Amazon_Storage_Gateway/', "Absolute URLs are not mangled");

done_testing;
