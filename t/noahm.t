#!/usr/bin/perl -w

use v5.28;
use utf8;
use strict;
use warnings;

use Encode qw/encode/;

use Test::More;
use Mojo::URL;
use Mojo::DOM;

use_ok('App::PtLink::Input');

my $url = Mojo::URL->new('file://./t/test-files/noahm.xml');

my $input = App::PtLink::Input->create("XmlFeed", url => $url);

isa_ok($input, "App::PtLink::Input");

my $messages = $input->messages;

isa_ok($messages, "ARRAY");
ok(scalar(@$messages) == 1, "The number of messages is parsed correctly");
my $message = $messages->[0];
isa_ok($message, "App::PtLink::Message");
ok($message->has_authors, "The message has authors");
my $author = $message->authors->[0];
ok($author->name eq "Noah Meyerhans", "The author name is parsed correctly");
ok($author->nick eq "frodo+blog", "The author's nickname is parsed correctly");
ok($author->email eq 'frodo+blog@morgul.net', "The author's email address is parsed correctly");

done_testing;
