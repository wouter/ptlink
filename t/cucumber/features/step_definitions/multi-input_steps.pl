#!/usr/bin/perl -w

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use App::PtLink::Input;

use Data::Dumper;

Given qr/source (?<number>[0-9]+), a MultiSource input with a "inputtype" property of "XmlFeed" and a "urllist" property as follows:/ => sub {
	my @urls = split(/\n/, C->data);
	S->{sources} = [] unless exists(S->{sources});
	S->{sources}[$+{number}] = App::PtLink::Input->create("MultiSource", inputtype => "XmlFeed", urllist => [@urls]);
};

Given qr/source (?<number>[0-9]+), a XmlFeed input with a "url" property set to "(?<url>[^"]+)"/ => sub {
	eval {
		S->{sources} = [] unless exists(S->{sources});
		S->{sources}[$+{number}] = App::PtLink::Input->create("XmlFeed", url => $+{url});
	};
	if($@) {
		fail("$@");
	}
};

When qr/we fetch the list of Messages from source (?<source>[0-9]+)/ => sub {
	S->{messages} = [] unless exists(S->{messages});
	S->{messages}[$+{source}] = [@{S->{sources}[$+{source}]->messages}];
	ok(scalar(@{S->{messages}[$+{source}]}) > 0, "we got a result");
};

Then qr/all the Message guids in all lists are identical/ => sub {
	my @collections = ( @{S->{messages}} );
	my $first;
	while(!defined($first)) {
		$first = shift(@collections);
	}
	ok(scalar(@collections) > 0);
	$first = [ sort map { $_->guid } @$first ];
	my $len = scalar(@$first);
	for my $collection(@collections) {
		ok(scalar(@$collection) == $len, "the length of $collection is fine");
		$collection = [ sort map { $_->guid } @$collection ];
		MESSAGE:
		for my $index(1..$len) {
			next MESSAGE unless (defined($collection->[$index]) && defined($first->[$index]));
			ok($collection->[$index] eq $first->[$index], "the guids match");
		}
	}
};

sub count_messages {
	my $expected = shift;
	my $total_count = 0;
	MESSAGE_LIST:
	foreach my $message_list(@{S->{messages}}) {
		next MESSAGE_LIST unless ref($message_list) eq "ARRAY";
		$total_count += scalar(@$message_list);
	}
	ok($total_count == $expected, "the number of messages matches");
}

Then qr/there are (?<count>[0-9]+) Messages/ => sub {
	return count_messages($+{count});
};

Then qr/there is no Message/ => sub {
	return count_messages(0);
};

1;
