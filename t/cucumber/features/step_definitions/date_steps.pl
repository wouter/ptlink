#!/usr/bin/perl -w

package App::PtLink::Output::Test;

use Moose;

extends 'App::PtLink::Output';

has 'S' => (
	is => 'ro',
	isa => 'CodeRef',
	required => 1,
);

sub commit_messages {
	my $self = shift;
	$self->S->()->{message} = $self->messages->[0];
};

no Moose;

package main;

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;
use DateTime;
use DateTime::Duration;
use DateTime::Format::ISO8601;

use_ok "App::PtLink::Input";
use_ok "App::PtLink::Output";
use_ok "App::PtLink::TimeStampSanitizer";

use_ok "App::PtLink::Message";

Given qr/a Message with a guid of "(?<guid>[^"]+)" and a timestamp of "(?<timestamp>[^"]+)"/ => sub {
	S->{message} = App::PtLink::Message->new(guid => $+{guid}, timestamp => DateTime::Format::ISO8601->parse_datetime($+{timestamp}));
};

Given qr/a Message with a guid of "(?<guid>[^"]+)" and a timestamp in the future/ => sub {
	S->{message} = App::PtLink::Message->new(guid => $+{guid}, timestamp => DateTime->now + DateTime::Duration->new(days => 1));
};

Given qr/(a|an) (?<type>\S+) input with a "(?<attribute>[^"]+)" attribute set to "(?<value>[^"]+)"/ => sub {
	S->{input} = App::PtLink::Input->create($+{type}, $+{attribute} => $+{value});
};

Then qr/there is no Message/ => sub {
	ok(!exists(S->{message}) || !defined(S->{message}), "the message does not exist or is not defined");
};

When qr/we add the Message to an Input/ => sub {
	S->{input} = App::PtLink::Input->new(messages => [S->{message}]);
};

When qr/we add the Input to a new TimeStampSanitizer/ => sub {
	ok(S->{tss} = App::PtLink::TimeStampSanitizer->new(chained_input => S->{input}, sanitizer_name => 'test'), "created an object");
};

When qr/we fetch the first Message from the TimeStampSanitizer/ => sub {
	S->{message} = S->{tss}->messages->[0];
};

When qr/we create a new Message with a guid of "(?<guid>[^"]+)" and a timestamp of "(?<timestamp>[^"]+)"/ => sub {
	C->dispatch("Given", 'a Message with a guid of "' . $+{guid} . '" and a timestamp of "' . $+{timestamp} . '"');
};

Then qr/the "timestamp" attribute on the Message is "(?<timestamp>[^"]+)"/ => sub {
	ok(DateTime->compare(S->{message}->timestamp, DateTime::Format::ISO8601->parse_datetime($+{timestamp})) == 0, "the timestamp matches");
};

When qr/we create a Test Output/ => sub {
	S->{output} = App::PtLink::Output->create("Test", S => \&S);
};

When qr/we add the Output to a new TimeStampSanitizer/ => sub {
	ok(S->{tss} = App::PtLink::TimeStampSanitizer->new(chained_output => S->{output}, sanitizer_name => 'test'), "created an object");
};

When qr/we add the Message to the TimeStampSanitizer/ => sub {
	S->{tss}->add_messages(S->{message});
};

When qr/we commit the Messages on the TimeStampSanitizer/ => sub {
	S->{tss}->commit_messages;
};

When qr/we set the "(?<attribute>[^"]+)" attribute on the TimeStampSanitizer to "(?<value>[^"]+)"/ => sub {
	S->{tss}->meta->get_attribute($+{attribute})->set_value(S->{tss}, $+{value});
};

Then qr/the TimeStampSanitizer has (?<count>\d+) messages/ => sub {
	ok(scalar(@{S->{tss}->messages}) == $+{count}, "the count matches");
};

1;
