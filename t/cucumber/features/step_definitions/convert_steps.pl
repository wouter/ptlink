#!/usr/bin/perl -w

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use_ok "App::PtLink::FormattedText";

Given qr/a FormattedText object with mimetype (\S+) and the following contents/ => sub {
	my $data = C->data;
	chomp $data;
	my $text = App::PtLink::FormattedText->new(mimetype => $1, text => $data);
	ok(defined($text), "the object was created");
	isa_ok($text, "App::PtLink::FormattedText");
	S->{text} = $text;
};

When qr/we ask for it as (\S+)/ => sub {
	eval {
		my $res = S->{text}->as($1);
		S->{converted} = "$res";
	};
	if ($@) {
		S->{exception} = $@;
	}
};

Then qr/the text is as follows/ => sub {
	my $data = C->data;
	chomp $data;
	my $target = S->{converted};
	ok($target eq $data, "\n|" . $target . "| equals \n|" . $data ."|");
};

Then qr/we have (an|no) exception/ => sub {
	if($1 eq "an") {
		ok(defined(S->{exception}));
	} else {
		ok(!defined(S->{exception}));
	}
};
