Feature: Timestamp retention
  As a consumer of a Message
  I want to have timestamps sanitized
  So that they are stable and not from the future

  Scenario: changing timestamps
      This scenario tests that dates which change do not cause posts to jump forwards and backwards
    Given a Message with a guid of "81b698d0-cd16-4cba-81cb-c64ed241c115" and a timestamp of "2023-07-09T13:15:11+02:00"
    When we add the Message to an Input
    And we add the Input to a new TimeStampSanitizer
    And we fetch the first Message from the TimeStampSanitizer
    And we create a new Message with a guid of "81b698d0-cd16-4cba-81cb-c64ed241c115" and a timestamp of "2023-07-10T13:15:11+02:00"
    And we add the Message to an Input
    And we add the Input to a new TimeStampSanitizer
    And we fetch the first Message from the TimeStampSanitizer
    Then the "timestamp" attribute on the Message is "2023-07-09T13:15:11+02:00"

  Scenario: future dates
      This scenario tests that posts which are dated in the future are ignored
    Given a Message with a guid of "fb33384c-e656-449a-950f-6f8d0426b5a1" and a timestamp in the future
    When we add the Message to an Input
    And we add the Input to a new TimeStampSanitizer
    And we fetch the first Message from the TimeStampSanitizer
    Then there is no Message

  Scenario: don't get all of them
      This scenario tests that when we try to read messages from an input with the "max messages" attribute set, we don't read everything
    Given an XmlFeed input with a "url" attribute set to "file://./t/test-files/ikiwiki-grep.atom"
    When we add the Input to a new TimeStampSanitizer
    And we set the "max_messages" attribute on the TimeStampSanitizer to "5"
    Then the TimeStampSanitizer has 5 messages

  @wip
  Scenario: update message count when required
      This scenario tests that updating the max_messages count reparses messages when necessary
    Given an XmlFeed input with a "url" attribute set to "file://./t/test-files/ikiwiki-grep.atom"
    When we add the Input to a new TimeStampSanitizer
    And we set the "max_messages" attribute on the TimeStampSanitizer to "5"
    And we fetch the first Message from the TimeStampSanitizer
    And we set the "max_messages" attribute on the TimeStampSanitizer to "8"
    Then the TimeStampSanitizer has 8 messages

  Scenario: output timestamps
      This scenario tests that the TimeStampSanitizer passes messages through while filtering them
    Given a Message with a guid of "ff559509-34e6-40fb-a276-26fd835e278e" and a timestamp of "2023-07-09T13:15:11+02:00"
    When we create a Test Output
    And we add the Output to a new TimeStampSanitizer
    And we add the Message to the TimeStampSanitizer
    And we commit the Messages on the TimeStampSanitizer
    And we add the Output to a new TimeStampSanitizer
    And we create a new Message with a guid of "ff559509-34e6-40fb-a276-26fd835e278e" and a timestamp of "2023-07-10T13:15:11+02:00"
    And we add the Message to the TimeStampSanitizer
    And we commit the Messages on the TimeStampSanitizer
    Then the "timestamp" attribute on the Message is "2023-07-09T13:15:11+02:00"
