Feature: Test of data conversion
  As a user of PtLink
  I want to convert messages from one format to another
  In order to copy them between otherwise-incompatible inputs and outputs

  Scenario: Get plain text through the system
      This scenario tests that a conversion to the same object type does not mangle the text
    Given a FormattedText object with mimetype text/plain and the following contents:
      """
      This is a plain text test
      """
    When we ask for it as text/plain
    Then the text is as follows:
      """
      This is a plain text test
      """
    And we have no exception

  Scenario: Convert HTML to text
      This scenario tests conversion of HTML to plain text
    Given a FormattedText object with mimetype text/html and the following contents:
      """
      \\<div\\>\\<p\\>This is a test, where we \\<i\\>know\\</i\\> we will lose all markup\\</p\\>\\</div\\>
      """
    When we ask for it as text/plain
    Then the text is as follows:
      """
      This is a test, where we know we will lose all markup
      """
    And we have no exception

  Scenario: Convert markdown to HTML
      This scenario tests conversion of Markdown to HTML
    Given a FormattedText object with mimetype text/markdown and the following contents:
      """
      This is a test

      * This is a test, too
      """
    When we ask for it as text/html
    Then the text is as follows:
      """
      \\<p\\>This is a test\\</p\\>

      \\<ul\\>
      \\<li\\>This is a test, too\\</li\\>
      \\</ul\\>

      """
    And we have no exception

  Scenario: Can't convert text to images
      This scenario tests that an impossible conversion throws an exception
    Given a FormattedText object with mimetype text/plain and the following contents:
      """
      This is a plain text test
      """
    When we ask for it as image/png
    Then we have an exception
