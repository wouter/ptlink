Feature: multi-source inputs
  As a developer of an aggregator using PtLink
  I want to have an Input component that pulls from multiple sources
  So that I don't have to reinvent the wheel.

  Scenario: Make sure that a single-source multi-source input works as expected
    Given source 1, a MultiSource input with a "inputtype" property of "XmlFeed" and a "urllist" property as follows:
       """
       file://./t/test-files/ikiwiki-grep.atom
       """
    And source 2, a XmlFeed input with a "url" property set to "file://./t/test-files/ikiwiki-grep.atom"
    When we fetch the list of Messages from source 1
    And we fetch the list of Messages from source 2
    Then all the Message guids in all lists are identical

  Scenario: Do not duplicate messages from the same source
    Given source 1, a MultiSource input with a "inputtype" property of "XmlFeed" and a "urllist" property as follows:
      """
      file://./t/test-files/ikiwiki-grep.atom
      file://./t/test-files/ikiwiki-grep.atom
      """
    When we fetch the list of Messages from source 1
    Then there are 10 Messages

  Scenario: Do not duplicate messages from the same source in a different format
    Given source 1, a MultiSource input with a "inputtype" property of "XmlFeed" and a "urllist" property as follows:
      """
      file://./t/test-files/ikiwiki-grep.atom
      file://./t/test-files/ikiwiki-grep-rss.xml
      """
    When we fetch the list of Messages from source 1
    Then there are 10 Messages
