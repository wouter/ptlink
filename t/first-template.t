#!/usr/bin/perl -w

use strict;
use warnings;

use feature 'say';

use Test::More;
use Mojo::URL;
use Mojo::DOM;

use_ok('App::PtLink::Input');
use_ok('App::PtLink::Output::Aggregator');
use_ok('App::PtLink::TemplateFactory');

my $url = Mojo::URL->new('file://./t/test-files/ikiwiki-grep.atom');
my $input = App::PtLink::Input->create("XmlFeed", url => $url);

my $aggregator = App::PtLink::Output::Aggregator->new;

$aggregator->limit(2);
$aggregator->add_messages(@{$input->messages});

my $template = App::PtLink::TemplateFactory->create('.tmpl');
$template->template_file('t/test-files/test.html.tmpl');

my $file = IO::File->new('test.html', '>:encoding(UTF-8)');
$aggregator->output_file($file);
$aggregator->template($template);
$aggregator->commit_messages;

$file->close;

my $parser;
{
	local $/ = undef;
	open my $file, "<", "test.html";
	$parser = Mojo::DOM->new(<$file>);
}

my $elems = $parser->find('p.first')->to_array;

ok(scalar (@{$elems}) == 1, "There is exactly one element with the first class");
ok($elems->[0]->text eq "First!", "The element renders correctly");

unlink("test.html");

done_testing;
