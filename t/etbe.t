#!/usr/bin/perl -w

use v5.28;
use utf8;
use strict;
use warnings;

use Encode qw/encode/;

use Test::More;
use Mojo::URL;
use Mojo::DOM;

use_ok('App::PtLink::Input');

my $url = Mojo::URL->new('file://./t/test-files/etbe.xml');

my $input = App::PtLink::Input->create("XmlFeed", url => $url);

isa_ok($input, "App::PtLink::Input");

my $messages = $input->messages;

isa_ok($messages, "ARRAY");
ok(scalar(@$messages) == 1, "The number of messages is parsed correctly");
isa_ok($messages->[0], "App::PtLink::Message");
ok($input->metadata->title eq "etbe \x{2013} Russell Coker", "The feed title is parsed correctly");

done_testing;
