#!/usr/bin/perl -w

use v5.28;
use strict;
use warnings;

use Test::More;
use Test::Moose;
use Test::Exception;

use_ok("App::PtLink::Filter::Defang");
use_ok("App::PtLink::Filter::Noop");
use_ok("App::PtLink::Filter::Stdio");
use_ok("App::PtLink::FormattedText");

my $text = App::PtLink::FormattedText->new(text => "<script>foo;</script><p>foo!</p>", mimetype => "text/html");
isa_ok($text, "App::PtLink::FormattedText");
my $defang = App::PtLink::Filter::Defang->new(text => $text);
isa_ok($defang, "App::PtLink::Filter::Defang");
does_ok($defang, "App::PtLink::Filter");
ok($defang->filtered_text->text eq "<p>foo!</p>", "the defang filter works");

$text->mimetype("text/plain");
throws_ok { $defang->filtered_text } qr/not in HTML format/, "an HTML-only filter will not accept text/plain";
$text->mimetype("text/html");

my $noop = App::PtLink::Filter::Noop->new(text => App::PtLink::FormattedText->new(text => "foo", mimetype => "text/plain"));
isa_ok($noop, "App::PtLink::Filter::Noop");
does_ok($noop, "App::PtLink::Filter");

ok($noop->filtered_text->text eq $noop->text->text, "The noop filter doesn't make changes");

$noop->next($defang);
$noop->clear_text();

ok($noop->filtered_text->text eq "<p>foo!</p>", "chaining the noop filter to the defang filter does what's expected");

my $stdio = App::PtLink::Filter::Stdio->new(text => App::PtLink::FormattedText->new(text => "foo baz", mimetype => "text/plain"), command => ["perl", "-pe", "s/baz/bar/g"], append_mime => 0);

ok($stdio->filtered_text->text eq "foo bar", "the stdio filter works");

my $broken = App::PtLink::Filter::Noop->new();
dies_ok {$broken->filtered_text} "a filter that can't work will die";

my $fail = App::PtLink::Filter::Stdio->new(text => App::PtLink::FormattedText->new(text => "foo", mimetype => "text/plain"), command => ["false"]);

dies_ok { $fail->filtered_text } "if the command fails, the stdio filter fails too";

my $change_mime = App::PtLink::Filter::Stdio->new(text => App::PtLink::FormattedText->new(text => "<p>foo</p>", mimetype => "text/html"), command => ["perl", "-pe", 'BEGIN{print "text/plain\n"}'], append_mime => 0, alters_mime => 1);
my $changed_output;

lives_ok {$changed_output = $change_mime->filtered_text} "we can fetch a filtered text without dying";

ok($changed_output->mimetype eq "text/plain", "the mime type is changed by the filter");

done_testing;
