#!/usr/bin/perl -w

use v5.28;
use utf8;
use strict;
use warnings;

use Test::More;
use Mojo::URL;
use Mojo::DOM;

use_ok('App::PtLink::Input');

my $url = Mojo::URL->new('file://./t/test-files/ploum.xml');

my $input = App::PtLink::Input->create("XmlFeed", url => $url);

isa_ok($input, "App::PtLink::Input");

my $messages = $input->messages;

isa_ok($messages, "ARRAY");
ok(scalar(@$messages) == 1, "The number of messages is parsed correctly");
isa_ok($messages->[0], "App::PtLink::Message");
ok($messages->[0]->title eq "Considérations sur le talent, le génie, le travail et un jeu vidéo que je vous recommande", "the title isn't mangled after parsing the feed");
like($messages->[0]->body->text, qr/.*é.*/, "the body isn't mangled after parsing the feed");

use_ok('App::PtLink::TemplateFactory');

my $template = App::PtLink::TemplateFactory->create(".tmpl");

$template->template_file('t/test-files/test.html.tmpl');
$template->set_param(Items => [ { link => "https://foo.bar", title => $messages->[0]->title, content => $messages->[0]->body->text } ]);

my $dom = Mojo::DOM->new($template->handle);

ok($dom->at('p.title')->text eq "Considérations sur le talent, le génie, le travail et un jeu vidéo que je vous recommande", "The template does not mangle the title");
ok($dom->at('h1')->text eq "Considérations sur le talent, le génie, le travail et un jeu vidéo que je vous recommande", "The template does not mangle the body");

done_testing;
