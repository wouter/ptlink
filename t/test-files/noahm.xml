<?xml version="1.0" encoding="utf-8" standalone="yes"?><rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom"><channel><title>Noah Meyerhans - debian</title><link>/categories/debian/</link><description>Recent content in debian on Noah Meyerhans</description><generator>Hugo -- gohugo.io</generator><language>en-us</language><managingEditor>frodo&#43;blog@morgul.net (Noah Meyerhans)</managingEditor><webMaster>frodo&#43;blog@morgul.net (Noah Meyerhans)</webMaster><lastBuildDate>Wed, 29 Dec 2021 09:49:13 -0800</lastBuildDate><atom:link href="/categories/debian/index.xml" rel="self" type="application/rss+xml"/><item><title>When You Could Hear Security Scans</title><link>/2021/12/29/when-you-could-hear-security-scans/</link><pubDate>Wed, 29 Dec 2021 09:49:13 -0800</pubDate><author>frodo&#43;blog@morgul.net (Noah Meyerhans)</author><guid>/2021/12/29/when-you-could-hear-security-scans/</guid><description>&lt;p&gt;Have you ever wondered what a security probe of a computer &lt;em&gt;sounded
like&lt;/em&gt;? I&amp;rsquo;d guess probably not, because on the face of it that doesn&amp;rsquo;t
make a whole lot of sense. But there was a time when I could very
clearly discern the sound of a computer being scanned. It sounded
like a small mechanical heart beat: &lt;em&gt;Click-click&amp;hellip;
click-click&amp;hellip; click-click&amp;hellip;&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;Prior to 2010, I had a computer under my desk with what at the time
were not unheard-of properties: Its storage was based on a stack of
spinning metal platters (a now-antiquated device known as a &amp;ldquo;hard
drive&amp;rdquo;), and it had a publicly routable IPv4 address with an
unfiltered connection to the Internet. Naturally it ran Linux and an
ssh server. As was common in those days, service logging was handled
by a syslog daemon. The syslog daemon would sort log messages based
on various criteria and record them somewhere. In most simple
environments, &amp;ldquo;somewhere&amp;rdquo; was simply a
&lt;a href=&#34;https://manpages.debian.org/bullseye/rsyslog/rsyslog.conf.5.en.html#Regular_file&#34;&gt;file on local storage&lt;/a&gt;.
When writing to a local file, syslog daemons can be optionally
configured to use the
&lt;a href=&#34;https://manpages.debian.org/bullseye/manpages-dev/fsync.2.en.html&#34;&gt;&lt;code&gt;fsync()&lt;/code&gt;&lt;/a&gt;
system call to ensure that writes are flushed to disk. Practically
speaking, what this meant is that a page of disk-backed memory would
be written to the disk as soon as an event occurred that triggered a
log message. Because of potential performance implications, &lt;code&gt;fsync()&lt;/code&gt;
was not typically enabled for most log files. However, due to the
more sensitive nature of authentication logs, it was often enabled for
&lt;code&gt;/var/log/auth.log&lt;/code&gt;.&lt;/p&gt;
&lt;p&gt;In the first decade of the 2000&amp;rsquo;s, there was a fairly unsophisticated
worm loose on the Internet that would
&lt;a href=&#34;https://arstechnica.com/civis/viewtopic.php?t=500295&#34;&gt;probe sshd&lt;/a&gt;
with some
&lt;a href=&#34;https://lin-web.clarkson.edu/~jmatthew/publications/leet08.pdf&#34;&gt;common username/password combinations&lt;/a&gt;.
The worm would pause for a second or so between login attempts, most
likely in an effort to avoid automated security responses. The effect
was that a system being probed by this worm would generate disk write
every second, with a very distinct audible signature from the hard
drive.&lt;/p&gt;
&lt;p&gt;I think this situation is a fun demonstration of a side-channel data
leak. It&amp;rsquo;s primitive and doesn&amp;rsquo;t leak very much information, but it
was certainly enough to make some inference about the state of the
system in question. Of course, side-channel leakage issues have been
a concern for ages, but I like this one for its simplicity. It was
something that could be explained and demonstrated easily, even to
somebody with relatively limited understanding of &amp;ldquo;how computers
work&amp;rdquo;, unlike, for instance
&lt;a href=&#34;https://www.cse.wustl.edu/~roger/566S.s21/09065580.pdf&#34;&gt;measuring electromagnetic emanations from CPU power management units&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;For a different take on the sounds of a computing infrastructure,
&lt;a href=&#34;https://www.usenix.org/legacy/publications/library/proceedings/lisa2000/full_papers/gilfix/gilfix_html/index.html&#34;&gt;Peep (The Network Auralizer)&lt;/a&gt;
won an award at a USENIX conference long, long ago. I&amp;rsquo;d love to see a
modern deployment of such a system. I&amp;rsquo;m sure you could build
something for your cloud deployment using something like
&lt;a href=&#34;https://aws.amazon.com/eventbridge/&#34;&gt;AWS EventBridge&lt;/a&gt; or
&lt;a href=&#34;https://aws.amazon.com/sqs/&#34;&gt;Amazon SQS&lt;/a&gt; fairly easily.&lt;/p&gt;
&lt;p&gt;For more on research into actual real-world side-channel attacks, you
can read
&lt;a href=&#34;https://arxiv.org/abs/2103.14244&#34;&gt;A Survey of Microarchitectural Side-channel Vulnerabilities, Attacks and Defenses in Cryptography&lt;/a&gt;
or
&lt;a href=&#34;https://arxiv.org/abs/1903.07703&#34;&gt;A Survey of Electromagnetic Side-Channel Attacks and Discussion on their Case-Progressing Potential for Digital Forensics&lt;/a&gt;.&lt;/p&gt;</description></item></channel></rss>
