#!/usr/bin/perl -w

use strict;
use warnings;

use Test::More;
use Mojo::URL;

use_ok('App::PtLink::Author');

my $url = Mojo::URL->new('https://grep.be/blog');
my $author = App::PtLink::Author->new(name => "test");
ok($author->string eq "test", "correct string with just an author name");
$author->nick("also test");
ok($author->string eq "test", "correct string with author name and nick");
$author->url($url);
ok($author->string eq "test", "correct string with author name, nick, and URL");
$author->email('test@example.com');
ok($author->string eq 'test (test@example.com)', "correct string with all data set");

$author = App::PtLink::Author->new(nick => "also test");
ok($author->string eq "also test", "correct string with only nick");
$author->email('test@example.com');
ok($author->string eq 'also test (test@example.com)', "correct string with nick and email");
$author->url($url);
ok($author->string eq 'also test (test@example.com)', "correct string with nick, email, and URL");

$author = App::PtLink::Author->new(url => $url);
ok($author->string eq "https://grep.be/blog", "correct string with just email");
$author->email('test@example.com');
ok($author->string eq "https://grep.be/blog", "email is ignored when only url is available");

$author = App::PtLink::Author->new();
ok(!defined($author->string), "when nothing is set, we have nothing");

done_testing;
