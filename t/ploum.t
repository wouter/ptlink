#!/usr/bin/perl -w

use strict;
use warnings;

use v5.28;

use Test::More;

use_ok "App::PtLink::Input";

my $input = App::PtLink::Input->create("XmlFeed", url => "file://./t/test-files/ploum.xml");

ok(defined($input), "we have an input");
ok(scalar(@{$input->messages}), "there are messages");
ok(scalar(@{$input->messages->[0]->authors}) == 1, "there is an author");
ok($input->messages->[0]->authors->[0]->name eq "Ploum", "The name of the first author is Ploum");

done_testing
