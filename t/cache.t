#!/usr/bin/perl -w

use strict;
use warnings;

use Test::More;
use Test::Moose;
use Mojo::URL;
use Moose::Util qw/apply_all_roles/;

use_ok('App::PtLink::Input');
use_ok('App::PtLink::CachableInput');
use_ok('App::PtLink::GlobalOptions', qw/set_global_option/);

set_global_option(cachedir => "./t");

symlink("test-files/ikiwiki-grep-rss.xml", "./t/input.xml");

my $url = Mojo::URL->new("file://./t/input.xml");
my $input = App::PtLink::Input->create("XmlFeed", url => $url);
apply_all_roles($input, "App::PtLink::CachableInput");
isa_ok($input, "App::PtLink::Input");
does_ok($input, "App::PtLink::CachableInput");
my $messages = $input->messages;

$input = App::PtLink::Input->create("XmlFeed", url => $url);
apply_all_roles($input, "App::PtLink::CachableInput");

ok(-f $input->cache_name, "The cache was created");

ok($input->_has_cache_for($url->to_string), "The cache can be read");

unlink("t/input.xml");

$input = App::PtLink::Input->create("XmlFeed", url => $url);
apply_all_roles($input, "App::PtLink::CachableInput");
$messages = $input->messages;

isa_ok($messages, "ARRAY");
isa_ok($messages->[0], "App::PtLink::Message");
ok($messages->[0]->title eq "Different types of Backups", "The first message parses correctly after reading stuff from cache");
ok(scalar(@$messages) == 10, "10 messages were parsed from the cache");

unlink($input->cache_name);

done_testing;
