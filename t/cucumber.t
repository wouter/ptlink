#!/usr/bin/perl -w

use strict;
use warnings;

use Test::BDD::Cucumber::Loader;
use Test::BDD::Cucumber::Harness::TAP;
use Cucumber::TagExpressions;
my $expr;

if(defined($ARGV[0]) && $ARGV[0] eq "wip-only") {
	$expr = Cucumber::TagExpressions->parse('@wip');
}

my ($executor, @features) = Test::BDD::Cucumber::Loader->load('t/cucumber/');
my $harness = Test::BDD::Cucumber::Harness::TAP->new({});

$executor->execute($_, $harness, $expr) for @features;

$harness->shutdown();
