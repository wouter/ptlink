#!/usr/bin/perl -w

use strict;
use warnings;

use Test::More;
use Mojo::URL;
use IO::File;
use Mojo::DOM;

use_ok('App::PtLink::Input');
use_ok('App::PtLink::Input::XmlFeed');
use_ok('App::PtLink::Output::Aggregator');
use_ok('App::PtLink::TemplateFactory');

no warnings("experimental::signatures");
use feature "signatures";

## no critic(SubroutinePrototypes)

sub check_file($filename, $module) {
	my $parser;
	{
		local $/ = undef;

		open my $file, "<", $filename;
		$parser = Mojo::DOM->new(<$file>);
	}

	my $coll = $parser->find('span.id')->to_array;

	ok($coll->[0]->text eq "https://grep.be/blog//en/computer/Different_types_of_Backups/", "$module: The text for the first message is the correct link");
}

my $aggregator = App::PtLink::Output::Aggregator->new;

$aggregator->limit(2);
$aggregator->set_attribute(generator => 'PtLink test suite');
my $url = Mojo::URL->new('file://./t/test-files/ikiwiki-grep.atom');
my $feed = App::PtLink::Input->create("XmlFeed", url => $url);
my $template = App::PtLink::TemplateFactory->create(".tmpl");
$template->template_file("t/test-files/test.html.tmpl");
$aggregator->template($template);
$aggregator->add_messages(@{$feed->messages});
ok(scalar(@{$aggregator->relevant_messages}) <= 2, "The messages are limited when requested");
my $file = IO::File->new("test.html", ">:encoding(utf8)");
$aggregator->output_file($file);
$aggregator->commit_messages;

$file->close;

check_file("test.html", "HTML::Template");

done_testing;
