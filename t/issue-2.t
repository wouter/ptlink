#!/usr/bin/perl -w

use v5.28;
use utf8;
use strict;
use warnings;

use Test::More;
use Mojo::URL;
use Mojo::DOM;

use_ok('App::PtLink::Input');

my $url = Mojo::URL->new('file://./t/test-files/valhalla.xml');

my $input = App::PtLink::Input->create("XmlFeed", url => $url);

isa_ok($input, "App::PtLink::Input");

my $messages = $input->messages;

isa_ok($messages, "ARRAY");
ok(scalar(@$messages) == 1, "The number of messages is parsed correctly");
my $message = $messages->[0];
isa_ok($message, "App::PtLink::Message");
ok(defined($message->body->text), "The message has text");
ok($message->body->text ne $message->title, "The message is not just the title");
ok(length($message->body->text) > length($message->title), "The message is longer than the title");

done_testing;
