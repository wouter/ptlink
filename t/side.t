#!/usr/bin/perl -w

use v5.28;
use utf8;
use strict;
use warnings;

use Encode qw/encode/;

use Test::More;
use Mojo::URL;
use Mojo::DOM;

use_ok('App::PtLink::Input');

my $url = Mojo::URL->new('file://./t/test-files/sid3windr.xml');

my $input = App::PtLink::Input->create("XmlFeed", url => $url);

isa_ok($input, "App::PtLink::Input");

my $messages = $input->messages;

isa_ok($messages, "ARRAY");
ok(scalar(@$messages) == 1, "The number of messages is parsed correctly");
isa_ok($messages->[0], "App::PtLink::Message");
ok($messages->[0]->title eq "Installing your centrally managed Let\x{2019}s Encrypt certificates with a Puppet module", "the title isn't mangled after parsing the feed");
like($messages->[0]->body->text, qr/.*\x{2019}.*/, "the body isn't mangled after parsing the feed");

use_ok('App::PtLink::TemplateFactory');

my $template = App::PtLink::TemplateFactory->create(".tmpl");

$template->template_file('t/test-files/test.html.tmpl');
$template->set_param(Items => [ { link => "https://foo.bar", title => $messages->[0]->title, content => $messages->[0]->body->text } ]);

my $dom = Mojo::DOM->new($template->handle);

ok($dom->at('p.title')->text eq "Installing your centrally managed Let\x{2019}s Encrypt certificates with a Puppet module", "The template does not mangle the title");
like($dom->find('div.content>p')->to_array->[0]->text, qr/.*\x{2019}.*/ , "The template does not mangle the body");

done_testing;
