package App::PtLink::IOObject;

use Moose;
use Moose::Util::TypeConstraints;

use Mojo::URL;

class_type 'Mojo::URL';

extends "App::PtLink::Base";

coerce "Mojo::URL",
	from "Str",
	via { Mojo::URL->new($_) };

has 'url' => (
	is => 'rw',
	isa => 'Mojo::URL',
	coerce => 1,
	predicate => 'has_url',
	trigger => sub {
		my $self = shift;
		if ($self->can("_clear_all_raw")) {
			$self->_clear_all_raw;
		}
	},
);

has 'messages' => (
	is => 'rw',
	isa => 'ArrayRef[App::PtLink::Message]',
	traits => ['Array'],
	handles => {
		add_messages => 'push',
		map_messages => 'map',
		sorted_messages => 'sort',
		filtered_messages => 'grep',
	},
	lazy_build => 1,
);

has 'metadata' => (
	is => 'rw',
	isa => 'App::PtLink::Metadata',
	lazy_build => 1,
);

no Moose;
1;
