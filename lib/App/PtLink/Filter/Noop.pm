package App::PtLink::Filter::Noop;

use Moose;

extends "App::PtLink::Base";

with "App::PtLink::Filter";

sub filtered_text {
	return shift->_get_unfiltered;
}

no Moose;

1;
