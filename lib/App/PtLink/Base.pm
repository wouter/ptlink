package App::PtLink::Base;

use Moose;

=head1 NAME

App::PtLink::Base - The base object that all other PtLink objects
extend.

=head1 ATTRIBUTES

=head2 attributes

The C<attributes> attribute is a hash that can contain string values. It
can be used by cooperating modules to add tags to messages, or to pass
metadata that isn't otherwise supported by PtLink.

=head3 methods

The following methods are available to manipulate the attribues:

=over

=item set_attribute



=cut

has 'attributes' => (
	is => 'ro',
	isa => 'HashRef[Str]',
	traits => ['Hash'],
	predicate => 'has_attributes',
	lazy_build => 1,
	handles => {
		set_attribute => 'set',
		get_attribute => 'get',
		has_no_attribute => 'is_empty',
		num_attributes => 'count',
		delete_attribute => 'delete',
		attribute_pairs => 'kv',
	},
);

sub _build_attributes { {} }

no Moose;

1;
