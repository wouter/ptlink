package App::PtLink::Author;

use Moose;

=head1 NAME

App::PtLink::Author - PtLink representation of a message author

=head1 DESCRIPTION

The Author module is used to store information on authors.

Note that not all fields are used by all input modules. The C<name>
field has the best chance of having any data at all, but nothing is
guaranteed.

=cut

extends 'App::PtLink::Base';

=head1 ATTRIBUTES

=head2 name

The "name" of the author, as given by the source. This field is also
used as a "fallback" field for author fields in input data that can't be
otherwise parsed.

=cut

has 'name' => (
	is => 'rw',
	isa => 'Str',
	trigger => \&clear_string,
	predicate => 'has_name',
);

=head2 nick

A nick name, or a user name, for the author, if one is available.

=cut

has 'nick' => (
	is => 'rw',
	isa => 'Str',
	trigger => \&clear_string,
	predicate => 'has_nick',
);

=head2 email

The email address of the author, if available.

=cut

has 'email' => (
	is => 'rw',
	isa => 'Str',
	trigger => \&clear_string,
	predicate => 'has_email',
);

=head2 url

The author's webpage, if one exists. Passed as a L<Mojo::URL>.

=cut

has 'url' => (
	is => 'rw',
	isa => 'Mojo::URL',
	trigger => \&clear_string,
	predicate => 'has_url',
);

has 'string' => (
	is => 'ro',
	lazy_build => 1,
);

sub _build_string {
	my $self = shift;
	my $rv;
	if($self->has_name) {
		$rv = $self->name;
	} elsif($self->has_nick) {
		$rv = $self->nick;
	} elsif($self->has_url) {
		return $self->url;
	}
	if($self->has_email) {
		$rv .= " (" . $self->email . ")";
	}
	return $rv;
}

no Moose;

1;
