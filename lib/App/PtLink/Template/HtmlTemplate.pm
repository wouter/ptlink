package App::PtLink::Template::HtmlTemplate;

use strict;
use warnings;

use Moose;
use HTML::Template;

extends 'App::PtLink::Template';

sub drop_comments {
	my $text = shift;
	$$text =~ s/^\s*#.*$//gm;
	$text;
}

sub handle {
	my $self = shift;
	my $output = shift;
	my $template = HTML::Template->new(filename => $self->template_file, die_on_bad_params => 0, filter => \&drop_comments);
	my $params = $self->params;
	$params->{Items}[0]{__FIRST__} = 1;
	$params->{Items}[-1]{__LAST__} = 1;
	$template->param(%$params);
	return $template->output;
}

sub extension {
	return "tmpl";
}

no Moose;

1;
