package App::PtLink::Template::MojoTemplate;

use Moose;

use Mojo::Template;

extends "App::PtLink::Template";

has '_engine' => (
	is => 'ro',
	isa => 'Mojo::Template',
	lazy => 1,
	default => sub { my $mt = Mojo::Template->new; $mt->vars(1); },
);

sub handle {
	my $self = shift;

	return $self->_engine->render_file($self->template_file, $self->params);
}

sub extension {
	return "ep";
}

no Moose;

1;
