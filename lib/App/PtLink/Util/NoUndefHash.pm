package App::PtLink::Util::NoUndefHash;

use strict;
use warnings;

use Tie::Hash;

our @ISA = qw(Tie::StdHash);

sub STORE {
	my ($self, $key, $value) = @_;

	if(ref($value) eq "ARRAY") {
		if(scalar(@$value) == 0) {
			$value = undef;
		}
	}
	if(defined($value)) {
		$self->{$key} = $value;
		return;
	}
	if(exists($self->{$key})) {
		delete $self->{$key};
	}
}
