package App::PtLink::FormattedText::Markdown;

use strict;
use warnings;

use Moose;
use Text::Markdown 'markdown';

extends 'App::PtLink::FormatPluginBase';

sub _build_source_formats {
	return {'text/markdown' => 1};
}

sub _build_target_formats {
	return {
		'text/html' => 1,
		'text/plain' => 1
	};
}

sub convert {
	my ($plugin, $ft, $target) = @_;
	if($target eq 'text/plain') {
		return $ft->text;
	}
	return markdown($ft->text);
}

no Moose;
1;
