package App::PtLink::FormattedText::HtmlFormatter;

use Moose;

extends 'App::PtLink::FormatPluginBase';

use HTML::FormatText;
use HTML::FormatMarkdown;
use HTML::FormatRTF;

sub _build_source_formats {
	return { 'text/html' => 1 };
}

sub _build_target_formats {
	return {
		'text/markdown' => 'HTML::FormatMarkdown',
		'application/rtf' => 'HTML::FormatRTF',
		'text/plain' => 'HTML::FormatText',
	};
}

sub convert {
	my ($self, $ft, $target) = @_;
	my $formatter = $self->get_convertor($target)->new(lm => 0);
	my $rv = $formatter->format_string($ft->text);
	chomp($rv);
	return $rv;
}

no Moose;
1;
