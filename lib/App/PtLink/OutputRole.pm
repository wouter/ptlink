package App::PtLink::OutputRole;

use App::PtLink::Base;
use App::PtLink::Message;
use App::PtLink::Metadata;
use Moose::Role;
use Mojo::URL;

requires 'commit_messages';
requires 'messages';
requires 'url';
requires 'metadata';

has 'relevant_messages' => (
	is => 'ro',
	isa => 'ArrayRef[App::PtLink::Message]',
	lazy_build => 1,
);

sub _build_relevant_messages {
	my $self = shift;
	return $self->messages;
}

has 'title' => (
	is => 'ro',
	isa => 'Str',
	predicate => 'has_title',
);

has 'description' => (
	is => 'ro',
	isa => 'Str',
	predicate => 'has_description',
);

has 'language' => (
	is => 'ro',
	isa => 'Str',
	predicate => 'has_language',
);

sub _build_messages {
	return [];
}

no Moose::Role;
1;
