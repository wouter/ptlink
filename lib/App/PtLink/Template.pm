package App::PtLink::Template;

use Moose;

extends 'App::PtLink::Base';

has 'template_file' => (
	is => 'rw',
	isa => 'Str',
);

has 'params' => (
	is => 'rw',
	isa => 'HashRef[Any]',
	traits => [ 'Hash' ],
	handles => {
		set_param => 'set',
		get_param => 'get',
		has_no_params => 'is_empty',
		delete_param => 'delete',
		param_pairs => 'kv',
	},
);

sub handle {
	...
}

no Moose;
1;
