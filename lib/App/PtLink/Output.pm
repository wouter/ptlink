package App::PtLink::Output;

use Module::Runtime qw/use_module/;
use Moose;
use App::PtLink::OutputRole;

extends 'App::PtLink::IOObject';

with 'App::PtLink::OutputRole';

sub create {
	my $class = shift;
	my $format = shift;
	my $options = [ @_ ];

	use_module "App::PtLink::Output::$format" or die $!;
	my $rv = "App::PtLink::Output::$format"->new(@$options);
	return $rv;
}

sub commit_messages {
	...
};

no Moose;
1;
