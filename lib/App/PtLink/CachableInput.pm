package App::PtLink::CachableInput;

use strict;
use warnings;

use Moose::Role;
use MLDBM qw/DB_File Storable/;
use App::PtLink::GlobalOptions qw/global_option/;
use DateTime::Format::HTTP;
use DateTime::Duration;
use DateTime;
use Mojo::URL;
use Fcntl;
use Crypt::xxHash qw/xxhash32_hex/;

my %db;

requires "original_url";

has 'cache_name' => (
	is => 'ro',
	isa => 'Str',
	lazy => 1,
	builder => '_build_cache_name',
);

sub _build_cache_name {
	return global_option('cachedir') . "/" . xxhash32_hex(shift->original_url->to_string, 0) . ".dbm";
}

has 'cache' => (
	is => 'ro',
	isa => 'HashRef',
	builder => '_build_cache',
	traits => ['Hash'],
	lazy => 1,
	handles => {
		_has_cache_for => 'exists',
	},
);

sub _build_cache {
	my $self = shift;
	my $rv = {};
	if(-f $self->cache_name) {
		tie (my %rdr, 'MLDBM', $self->cache_name, O_CREAT|O_RDWR, 0640) or die $!; ## no critic(ProhibitLeadingZeros)
		$rv = { %rdr };
		untie %rdr;
	}
	return $rv;
}

sub _build_raw {
	my $self = shift;
	my $o = {data => undef};
	my $url = $self->url->to_string;
	if($self->_has_cache_for($url)) {
		$o = $self->cache->{$url};
		if(exists($o->{redirect_to})) {
			$self->url(Mojo::URL->new($o->{redirect_to}));
			return $self->_build_raw;
		}
		if($self->skip_online || (exists($o->{expirydate}) && $o->{expirydate} > DateTime->now())) {
			return $o->{data};
		}
		if(exists($o->{last_change})) {
			$self->last_change($o->{last_change});
		}
	}
	my $rv;
	eval {$rv = $self->_raw_uncached};
	if(!defined($rv)) {
		if(exists($o->{data}) && defined($o->{data})) {
			return $o->{data};
		}
		die $@;
	}
	return $rv;
}

sub DEMOLISH {
	my $self = shift;
	tie (my %db, 'MLDBM', $self->cache_name, O_CREAT|O_RDWR, 0640) or die $!; ## no critic(ProhibitLeadingZeros)
	if($self->has_raw && ($self->has_last_change || $self->has_expirydate)) {
		my $o = {};
		$o->{data} = $self->raw;
		if($self->original_url ne $self->url && $self->should_cache_redirect) {
			my $r = {redirect_to => $self->url->to_string};
			$db{$self->original_url->to_string} = $r;
		}
		if($self->has_last_change) {
			$o->{last_change} = $self->last_change;
		}
		if($self->has_expirydate) {
			$o->{expirydate} = $self->expirydate;
		}
		$db{$self->url->to_string} = $o;
	}
	untie %db;
}

1;
