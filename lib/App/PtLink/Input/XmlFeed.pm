package App::PtLink::Input::XmlFeed;

use v5.28;
use XML::Feed;
use Moose;

use Encode qw/decode/;

use App::PtLink::Util::NoUndefHash;
use App::PtLink::FormattedText;
use App::PtLink::Message;
use App::PtLink::Author;
use Email::Address;
use File::Type;
use Mojo::DOM;

my $ft = File::Type->new();

extends "App::PtLink::Input";

has 'xmlfeed' => (
	is => 'rw',
	isa => 'XML::Feed',
	lazy_build => 1,
);

sub _clear_all_raw {
	my $self = shift;
	$self->clear_xmlfeed;
	$self->SUPER::_clear_all_raw;
}

sub _build_input_type {
	return "xmlfeed";
}

sub safe_decode {
	my ($encoding, $string) = @_;
	my $rv;
	eval {
		$rv = decode($encoding, $string, Encode::FB_CROAK);
	};
	if($@) {
		$rv = $string;
	}
	return $rv;
}

sub _build_xmlfeed {
	my $self = shift;
	my $data = $self->raw;
	my $base = $self->dom->at("head base");
	if($ft->checktype_contents($data) eq "text/html") {
		if(defined($base)) {
			$base = Mojo::URL->new($base->{href});
		} else {
			$base = $self->url;
		}
		foreach my $link($self->dom->find('head link')->each) {
			if(exists($link->{rel})
					&& $link->{rel} eq "alternate"
					&& exists($link->{type})
					&& grep { $link->{type} eq $_ }
						qw|application/rss+xml application/atom+xml application/rdf+xml application/xml text/xml|) {
				$self->url(Mojo::URL->new($link->{href})->base($base)->to_abs);
				return $self->_build_xmlfeed;
			}
		}
		die "The content at " . $self->url . " contains HTML with no links to an XML feed. I can't parse this.\n"
	}
	$data =~ s/^\s*//;
	my $feed = XML::Feed->parse(\$data) or die "XML::Feed failed to parse: " . XML::Feed->errstr . "\n";
	return $feed;
}

sub _build_metadata {
	my $self = shift;
	tie my %args, 'App::PtLink::Util::NoUndefHash';
	$args{title} = safe_decode($self->encoding, $self->xmlfeed->title);
	$args{link} = $self->xmlfeed->link;
	$args{description} = safe_decode($self->encoding, $self->xmlfeed->description);
	$args{authors} = [ map{
		tie my %aargs, 'App::PtLink::Util::NoUndefHash';
		$aargs{name} = safe_decode($self->encoding, $_->name);
		$aargs{nick} = safe_decode($self->encoding, $_->user);
		$aargs{email} = $_->address;
		App::PtLink::Author->new(%aargs) } Email::Address->parse($self->xmlfeed->author) ];
	if(!exists($args{authors}) && defined($self->xmlfeed->author) && length($self->xmlfeed->author) > 0) {
		$args{authors} = [ App::PtLink::Author->new(name => $self->xmlfeed->author) ];
	}
	return App::PtLink::Metadata->new(%args);
}

sub _build_messages {
	my $self = shift;
	my $rv = [];
	my $base = $self->xmlfeed->base;
	if(!defined($base)) {
		$base = $self->xmlfeed->link;
	}
	if(defined($base)) {
		$base = Mojo::URL->new($base);
	}
	foreach my $entry ($self->xmlfeed->entries) {
		last if($self->max_messages > 0 && (scalar(@$rv)) >= $self->max_messages);
		tie my %margs, 'App::PtLink::Util::NoUndefHash';
		my $title = safe_decode($self->encoding, $entry->title);
		my $text = defined($entry->content->body) ? safe_decode($self->encoding, $entry->content->body) : (defined($entry->summary->body) ? safe_decode($self->encoding, $entry->summary->body) : $title);
		if(defined($base)) {
			my $dom = Mojo::DOM->new($text);
			$dom->find('img[src]')->map(sub {
				$_->attr(src => Mojo::URL->new($_->attr('src'))->base($base)->to_abs->to_string);
			});
			$dom->find('a[href]')->map(sub {
				$_->attr(href => Mojo::URL->new($_->attr('href'))->base($base)->to_abs->to_string);
			});
			$text = "$dom";
		}
		my %fargs = (
			text => $text,
			mimetype => (defined($entry->content->type) ? $entry->content->type : 'text/html'),
		);
		next unless defined($fargs{text});
		$margs{body} = App::PtLink::FormattedText->new(%fargs);
		$margs{title} = $title;
		$margs{url} = Mojo::URL->new($entry->link);
		$margs{authors} = [ map {
			tie my %aargs, 'App::PtLink::Util::NoUndefHash';
			$aargs{name} = safe_decode($self->encoding, $_->name);
			$aargs{nick} = safe_decode($self->encoding, $_->user);
			$aargs{email} = $_->address;
			App::PtLink::Author->new(%aargs) } Email::Address->parse($entry->author)];
		if(!exists($margs{authors}) && defined($entry->author)) {
			$margs{authors} = [ App::PtLink::Author->new(name => $entry->author) ];
		}
		if(!exists($margs{authors}) && defined($self->xmlfeed->author) && length($self->xmlfeed->author) > 0) {
			$margs{authors} = [ App::PtLink::Author->new(name => safe_decode($self->encoding, $self->xmlfeed->author)) ];
		}
		$margs{timestamp} = defined($entry->issued) ? $entry->issued : $entry->modified;
		$margs{guid} = $entry->id;
		$margs{source} = $self;
		push @$rv, App::PtLink::Message->new(%margs);
	}
	return $rv;
}

no Moose;

1;
