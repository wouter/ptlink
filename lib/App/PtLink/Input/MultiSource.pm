package App::PtLink::Input::MultiSource;

use Moose;
use Moose::Util::TypeConstraints;
use App::PtLink::InputRole;
use DateTime;
use Data::Dumper;

extends 'App::PtLink::Input';

has 'urllist' => (
	is => 'rw',
	isa => 'ArrayRef[Str]',
);

has 'inputtype' => (
	is => 'rw',
	isa => 'Str',
);

has 'inputs' => (
	is => 'rw',
	isa => 'ArrayRef[App::PtLink::InputRole]',
	traits => ['Array'],
	handles => {
		add_input => 'push',
	},
	builder => '_build_inputs',
	lazy => 1,
);

sub _build_inputs {
	my $self = shift;
	my $rv = [];
	foreach my $url(@{$self->urllist}) {
		push @$rv, $self->create($self->inputtype, url => $url);
	}
	return $rv;
}

sub _build_messages {
	my $self = shift;
	my %seen;
	my $rv = [ grep { !$seen{$_->guid}++ } reverse sort { DateTime->compare($a->timestamp, $b->timestamp) } map { @{$_->messages} } @{$self->inputs} ];
	#my $rv = [ map { @{$_->messages} } @{$self->inputs} ];

	if($self->max_messages > 0) {
		splice @$rv, 0, $self->max_messages;
	}
	return $rv;
}

sub _clear_all_raw {
	my $self = shift;
	foreach my $input(@{$self->inputs}) {
		$self->_clear_all_raw;
	}
}

has '+skip_online' => (
	trigger => '_pass_skip_online',
);

sub _pass_skip_online {
	my ($self, $new, $old) = @_;
	foreach my $input(@{$self->inputs}) {
		$input->skip_online($new);
	}
}

no Moose;

1;
