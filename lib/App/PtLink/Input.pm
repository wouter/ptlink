package App::PtLink::Input;

use Moose;

extends 'App::PtLink::IOObject';

with 'App::PtLink::InputRole';

sub create {
	my $class = shift;
	my $format = shift;
	my $options = \@_ ;

	eval "require App::PtLink::Input::$format;" or die $!; ## no critic(StringyEval)
	my $rv = "App::PtLink::Input::$format"->new(@$options);
	return $rv;
}

no Moose;
1;
