package App::PtLink::TemplateFactory;

use v5.28;

use MooseX::Singleton;
use Module::Pluggable search_path => "App::PtLink::Template", sub_name => "_plugins";
use File::Basename;

has 'extensions' => (
	isa => 'HashRef[Str]',
	traits => ['Hash'],
	is => 'ro',
	handles => {
		template_for_extension => 'get',
		have_template => 'exists',
		supported_templates => 'keys',
	},
	lazy => 1,
	builder => '_build_extensions',
);

has 'plugins' => (
	isa => 'ArrayRef[Str]',
	is => 'ro',
	lazy => 1,
	builder => '_build_plugins',
);

sub _build_extensions {
	my $self = shift;
	my $rv = {};
	foreach my $plugin(@{$self->plugins}) {
		eval "require $plugin"; ## no critic(StringyEval);
		if($@) {
			say "skipping unloadable plugin $plugin: $@";
			next;
		}
		next unless $plugin->can("extension");
		$rv->{"." . $plugin->extension} = $plugin;
	}
	return $rv;
}

sub _build_plugins {
	return [ shift->_plugins ];
}

sub create {
	my ($self, $suffix) = @_;
	die "unknown template type $suffix" unless $self->have_template($suffix);
	return $self->template_for_extension($suffix)->new;
}

no Moose;

1;
