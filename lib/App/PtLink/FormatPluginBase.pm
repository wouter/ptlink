package App::PtLink::FormatPluginBase;

use Moose;

has 'source_formats' => (
	is => 'ro',
	isa => 'HashRef[Any]',
	traits => ['Hash'],
	handles => {
		parses => 'exists',
	},
	lazy => 1,
	builder => '_build_source_formats',
);

has 'target_formats' => (
	is => 'ro',
	isa => 'HashRef[Any]',
	traits => ['Hash'],
	handles => {
		converts_to => 'exists',
		get_convertor => 'get',
	},
	lazy => 1,
	builder => '_build_target_formats',
);

no Moose;

1;
