package App::PtLink::Output::Aggregator;

use Moose;
use IO::Handle;

use App::PtLink::Template;
use App::PtLink::Filter::Noop;

extends "App::PtLink::Output";

has 'limit' => (
	is => 'rw',
	isa => 'Int',
	default => 60,
);

has 'template' => (
	is => 'rw',
	isa => 'App::PtLink::Template',
	predicate => 'has_template',
);

has 'blocklist' => (
	is => 'rw',
	isa => 'HashRef[Any]',
	predicate => 'has_blocklist',
	trigger => \&_bl_changed, 
);

has '_blocked_guids' => (
	is => 'bare',
	lazy_build => 1,
	traits => ['Hash'],
	handles => {
		get_guid_blocklist => 'get',
		has_guid_blocklist => 'exists',
	},
);

sub _build__blocked_guids {
	my $self = shift;
	if(!$self->has_blocklist) {
		return {};
	}
	my $rv = {};
	foreach my $source(keys %{$self->blocklist}) {
		next unless exists($self->blocklist->{$source}{guids});
		$rv->{$source} = {};
		foreach my $guid(@{$self->blocklist->{$source}{guids}}) {
			$rv->{$source}{$guid} = 1;
		}
	}
	return $rv;
}

has '_blocked_urls' => (
	is => 'bare',
	lazy_build => 1,
	traits => ['Hash'],
	handles => {
		get_url_blocklist => 'get',
		has_url_blocklist => 'exists',
	},
);

sub _build__blocked_urls {
	my $self = shift;
	if(!$self->has_blocklist) {
		return {};
	}
	my $rv = {};
	foreach my $source(keys %{$self->blocklist}) {
		next unless exists($self->blocklist->{$source}{urls});
		$rv->{$source} = {};
		foreach my $url(@{$self->blocklist->{$source}{urls}}) {
			$rv->{$source}{$url} = 1;
		}
	}
	return $rv;
}

sub _msgs_changed {
	my $self = shift;
	$self->clear_relevant_messages;
	$self->_clear_template_params;
}

sub _bl_changed {
	my $self = shift;
	$self->_msgs_changed;
	$self->_clear_blocked_guids;
	$self->_clear_blocked_urls;
}

sub _build_relevant_messages {
	my $self = shift;

	local $_;
	my $sources = {};

	my @messages;
	if(!$self->has_blocklist) {
		@messages = sort({
			$b->timestamp <=> $a->timestamp
		} $self->filtered_messages(sub {
			$sources->{$_->source->url} = $_->source;
			defined($_) && $_->has_timestamp
		}));
	} else {
		@messages = sort({
			$b->timestamp <=> $a->timestamp
		} $self->filtered_messages(sub {
			$sources->{$_->source->url} = $_->source;
			my $surl = $_->source->url;
			my $rv = defined($_) && $_->has_timestamp;
			if($rv && $self->has_guid_blocklist($surl)) {
				my $blocklist = $self->get_guid_blocklist($surl);
				if(exists($blocklist->{$_->guid})) {
					$rv = 0;
				}
			}
			if($rv && $self->has_url_blocklist($surl)) {
				my $blocklist = $self->get_url_blocklist($surl);
				if(exists($blocklist->{$_->url})) {
					$rv = 0;
				}
			}
			$rv;
		}));
	}
	$self->channels($sources);
	if(scalar(@messages) < $self->limit) {
		return [ @messages ];
	} else {
		return [ @messages[0..$self->limit - 1] ];
	}
}

has '+messages' => (
	trigger => \&_msgs_changed,
);

has '_template_params' => (
	is => 'rw',
	isa => 'HashRef[Any]',
	traits => ['Hash'],
	handles => {
		_set_tmpl_param => 'set',
	},
	lazy_build => 1,
);

sub _build__template_params {
	my $self = shift;
	my $rv = {};
	my $last_message = undef;
	my $first_filter;
	my $last_filter;

	local $_;

	if($self->has_filters) {
		my $filters = $self->filters;
		$first_filter = $filters->[0];
		$last_filter = $filters->[$#{$filters}];
		my $prev_filter = $first_filter;
		foreach my $i(1..$#{$filters}) {
			$filters->[$i]->next($prev_filter);
			$prev_filter = $filters->[$i];
		}
	} else {
		$first_filter = App::PtLink::Filter::Noop->new;
		$last_filter = $first_filter;
	}
	$rv->{Items} = [ map {
		my %channel_attributes;
		foreach my $pair($_->source->attribute_pairs) {
			$channel_attributes{"channel_" . $pair->[0]} = $pair->[1];
		}
		my $hash = {
			%{$_->attributes},
			%channel_attributes,
			link => $_->url,
			content => $_->body->as("text/html"),
			id => $_->guid,
			%{$self->fetch_item_params($self, $_, $last_message)},
		};
		$last_message = $_;
		$hash;
	} map {
		$first_filter->text($_->body);
		bless { %$_, body => $last_filter->filtered_text }, ref $_;
	} @{$self->relevant_messages} ];
	# XXX Planet Venus sorts the Channels array by the order in
	# the configuration file.
	#
	# PtLink does not support this, due to the fact that Config::INI
	# turns the configuration file into a hash. So we sort
	# explicitly.
	#
	# TODO: allow configuring the sort order, if someone wants that.
	$rv->{Channels} = [ sort {
		$a->{name} cmp $b->{name}
	} map {
		my $channel = $self->get_channel($_);
		my $hash = {
			%{$channel->attributes},
			%{$self->fetch_channel_params($self, $channel)},
		};
		$hash;
	} keys %{$self->channels} ];
	return $rv;
}

has 'output_file' => (
	is => 'rw',
	isa => 'IO::Handle',
	default => sub { return IO::Handle->new_from_fd(fileno(STDOUT), "w") },
	lazy => 1,
);

# This needs to be a sub that returns a sub, because of how Moose works. Yah.
sub _return_empty_hash {
	return sub { {} };
}

has 'extra_item_params' => (
	is => 'rw',
	isa => 'CodeRef',
	traits => ['Code'],
	required => 1,
	default => \&_return_empty_hash,
	handles => {
		fetch_item_params => 'execute',
	},
);

has 'extra_channel_params' => (
	is => 'rw',
	isa => 'CodeRef',
	traits => ['Code'],
	required => 1,
	default => \&_return_empty_hash,
	handles => {
		fetch_channel_params => 'execute',
	},
);

has 'config_params' => (
	is => 'rw',
	isa => 'HashRef[Any]',
	default => sub { {} },
	lazy => 1,
	traits => ['Hash'],
	handles => {
		num_config_params => 'count',
	},
);

has filters => (
	is => 'rw',
	isa => 'ArrayRef[App::PtLink::Filter]',
	predicate => 'has_filters',
	trigger => \&_msgs_changed,
	traits => ['Array'],
	handles => {
		add_filter => 'push',
	},
);

has 'channels' => (
	is => 'rw',
	isa => 'HashRef[App::PtLink::InputRole]',
	traits => ['Hash'],
	handles => {
		set_channel => 'set',
		get_channel => 'get',
		channel_pairs => 'kv',
	},
);

sub commit_messages {
	my $self = shift;

	die "template not set" unless $self->has_template;

	$self->template->set_param(%{$self->attributes}) if ($self->num_attributes > 0);
	$self->template->set_param(%{$self->_template_params});
	$self->template->set_param(%{$self->config_params}) if ($self->num_config_params > 0);
	$self->template->set_param(Config => $self->config_params);
	$self->output_file->print($self->template->handle($self, $self->relevant_messages));
}

no Moose;
1;

