package App::PtLink::TimeStampSanitizer;

use strict;
use warnings;

use v5.28;

use Moose;
use MLDBM qw/DB_File Storable/;
use Crypt::xxHash qw/xxhash32_hex/;
use DateTime;
use Fcntl;

use App::PtLink::GlobalOptions qw/global_option/;

use feature "signatures";
no warnings "experimental::signatures";

## no critic(ProhibitSubroutinePrototypes)

extends 'App::PtLink::IOObject';

with 'App::PtLink::InputRole';
with 'App::PtLink::OutputRole';

has 'chained_input' => (
	is => 'ro',
	does => 'App::PtLink::InputRole',
	predicate => 'has_chained_input',
);

has 'chained_output' => (
	is => 'ro',
	does => 'App::PtLink::OutputRole',
	predicate => 'has_chained_output',
);

has 'sanitizer_name' => (
	is => 'ro',
	isa => 'Str',
	required => 1,
);

has '_db' => (
	is => 'ro',
	isa => 'HashRef[Int]',
	lazy => 1,
	builder => '_build_db',
	traits => ['Hash'],
	handles => {
		_db_pairs => 'kv',
		_db_timestamp_for => 'get',
		_db_set_timestamp => 'set',
		_db_has_timestamp => 'exists',
	},
);

has '+max_messages' => (
	trigger => sub {
		my $self = shift;
		return $self->clear_messages if $self->has_chained_input;
	},
);

sub _build_db {
	my $self = shift;

	return {} unless -f $self->_db_name;

	tie my %db, 'MLDBM', $self->_db_name, O_RDONLY, 0640 or die $!; ## no critic(ProhibitLeadingZeros)
	return { %db };
}

has '_db_name' => (
	is => 'ro',
	isa => 'Str',
	lazy => 1,
	builder => '_build_db_name',
);

has '_is_inited' => (
	is => 'rw',
	isa => 'Bool',
	default => 0,
);

sub _build_db_name {
	return global_option('cachedir') . "/" . xxhash32_hex(shift->sanitizer_name, 0) . ".dbm";
}

sub BUILD {
	my $self = shift;
	die "Need at least a chained input or a chained output" unless $self->has_chained_output || $self->has_chained_input;
	$self->_is_inited(1);
}

sub _ts_checked_message($self, $message, $now) {
	if($self->_db_has_timestamp($message->guid)) {
		$message->timestamp(DateTime->from_epoch(epoch => $self->_db_timestamp_for($message->guid)));
	}
	if($message->timestamp > $now) {
		return;
	}
	$self->_db_set_timestamp($message->guid, $message->timestamp->epoch);
	return $message;
}

sub commit_messages {
	my $self = shift;

	die "can't commit messages without an output" unless $self->has_chained_output;

	my $now = DateTime->now();
	$self->chained_output->clear_messages;
	foreach my $message(@{$self->messages}) {
		$message = $self->_ts_checked_message($message, $now);
		$self->chained_output->add_messages($message) if defined($message);
	}
	$self->chained_output->commit_messages;
}

sub _build_messages($self) {
	return [] unless $self->has_chained_input;
	my $now = DateTime->now();
	my $rv = [];
	MESSAGE:
	foreach my $message(@{$self->chained_input->messages}) {
		$message = $self->_ts_checked_message($message, $now);
		push @$rv, $message if defined($message);
		last MESSAGE if ($self->has_max_messages && scalar(@$rv) > $self->max_messages);
	}
	if($self->max_messages > 0) {
		$rv = [ splice @$rv, 0, $self->max_messages ];
	}
	return $rv;
}

sub DEMOLISH {
	my $self = shift;
	return unless $self->_is_inited;

	tie (my %db, 'MLDBM', $self->_db_name, O_CREAT|O_RDWR, 0640) or die $!; ## no critic(ProhibitLeadingZeros)
	foreach my $pair ($self->_db_pairs) {
		$db{$pair->[0]} = $pair->[1];
	}
	untie %db;
}

1;
