package App::PtLink::FormattedText;

use Moose;
use Module::Pluggable search_path => "App::PtLink::FormattedText", instantiate => 'new';

extends 'App::PtLink::Base';

has 'text' => (
	is => 'rw',
	required => 1,
	isa => 'Str',
);

has 'mimetype' => (
	is => 'rw',
	required => 1,
	isa => 'Str',
);

sub as {
	my $self = shift;
	my $wanted = shift;
	if($self->mimetype ne $wanted) {
		foreach my $plugin($self->plugins) {
			if($plugin->parses($self->mimetype) && $plugin->converts_to($wanted)) {
				return $plugin->convert($self, $wanted);
			}
		}
		die "could not convert " . $self->mimetype . " to $wanted: no sufficient plugin found";
	}
	return $self->text;
}

no Moose;
1;
