package App::PtLink::GlobalOptions;

use strict;
use warnings;

use Exporter;

our @ISA = qw/Exporter/;

our @EXPORT_OK = qw/global_option set_global_option/;

use Carp;

my %options = (
	cachedir => "/tmp",
);

sub global_option {
	my $opt = shift;
	croak "unknown option $opt" unless exists($options{$opt});

	return $options{$opt};
}

sub set_global_option {
	while(defined $_[0]) {
		my $opt = shift;
		my $val = shift;
		$options{$opt} = $val;
	}
}
