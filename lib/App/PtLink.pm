package App::PtLink;

use strict;
use warnings;

our $VERSION;

$VERSION = "0.0.1";

=head1 NAME

PtLink - a toolkit to convert social media data from one format to another

=head1 DESCRIPTION

Conceptually, PtLink is a toolkit to handle messages flowing from an
Input source to an Output dump.

Messages can be a number of things: blog posts, Twitter tweets, Facebook
posts, Mastodon toots, etc. Each message will have an author, a body of
text, and a bunch of metadata attached to it.

To use the toolkit, one would first create an input source, fetch
messages from them, possibly mangle them somehow, and then post them to
an output.

Alternatively, one can also create messages from scratch and post them
to the output.

=head2 EXAMPLE

  my $feed1 = App::PtLink::Input->create("XmlFeed", url => Mojo::URL->new("https://grep.be/blog/index.rss"));
  my $feed2 = App::PtLink::Input->create("XmlFeed",
    url => Mojo::URL->new("https://planet.grep.be/rss20.xml"), max_messages => 10);

  my $output = App::PtLink::Output->create("Aggregator",
    template => App::PtLink::Template::HtmlTmpl->new(template_file => "foo.html.tmpl"),
    output_file => IO::File->new("foo.html"),
    messages => [ $feed1->messages, $feed2->messages ]
  );

  $output->commit_messages;

=cut
