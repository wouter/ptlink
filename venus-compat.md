# Planet Venus compatibility

One of the first goals of the PtLink project is to provide [Planet
Venus](https://intertwingly.net/code/venus) compatibility. The aim here
is not to be 100% equal, but instead to be a functional replacement,
which provides output that is similar.

Nevertheless, there are some functional differences between Planet Venus
and ptlink-planet. This doucment tries to enumerate them.

If you find some difference that is not listed in this document, then
that is a bug -- at the very least the lack of documentation in this
document is ;-)

## Channel list contents

The `Channels` variable in Planet Venus contains *all* channels that
were configured in Planet Venus' configuration file. If the URL for a
channel cannot be reached, or does not contain any data, then Planet
Venus will still have the channel, but the URL fields for the feed and
for the channel will be empty.

ptlink-planet instead drops the channel from the `Channels` variable
entirely. It is believed that if a feed 404s, it should not be shown
anymore anyway.

Note that if a feed did show some data at some point in the past, this
will be stored in the cache, and then PtLink will still see data for
that feed. As such, temporary disappearances of data should not cause a
feed to flip-flop between being present and not being present.

## Channel sort order

Planet Venus sorts items in the `Channels` variable by the order in
which they appear in `config.ini`.

Since
[`Config::INI::Reader`](https://metacpan.org/pod/Config::INI::Reader)'s
API does not provide for the option to retain this ordering,
ptlink-planet instead forces an alphabetical ordering on the feed name.
